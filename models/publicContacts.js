const mongoose = require('mongoose');

const publicContactsSchema = new mongoose.Schema({
  emailId: String,
  engagementIntelligence: [{
    type: String
  }],
  connectionsFrom: [{
    name: {
      type: String,
      enum: ['gmail', 'linkedIn', 'twitter', 'apple', 'outlook']
    }
  }],
  relationship: String,
  professions: [{
    company: String,
    designation: String,
    description: String,
    startDate: String,
    endDate: String
  }],
  favorite: {
    type: Boolean,
    default: false
  },
  indexLabels: [{type: String}],
  twitter: {
    handle: String,
    profileUrl: String,
    recentTweet: {
      link: String,
      title: String,
      date: Date 
    }
 },
  linkedIn: {
    profileUrl: String,
    recentPost: {
      link: String,
      title: String,
      date: Date 
    }
  }
}, { timestamps: true });

const PublicContactsSchema = mongoose.model('publicContacts', publicContactsSchema, 'publicContacts');
module.exports = PublicContactsSchema;