const mongoose = require('mongoose');

const privateContactsSchema = new mongoose.Schema({
  firstName: String, //index
  lastName: String,//index
  emailId: String,//index
  mobileNumber: String,//index
  location: {
    state: String,//index
    country: String,//index
    city: String,//index
    latitude: String,
    longitude: String
  },
  company: String,//index
  notes: [{//index
    text: String,
    date: Date
  }],
  bio: String,//index
  tags: [{type: String}], //index
}, { timestamps: true });

const PrivateContacts = mongoose.model('privateContacts', privateContactsSchema, 'privateContacts');
module.exports = PrivateContacts;