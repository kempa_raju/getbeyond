const mongoose = require('mongoose');

const contactsSchema = new mongoose.Schema({
  firstName: String, //index
  lastName: String,//index
  emailId: String,//index
  ownerEmailId: String,//index
  mobileNumber: String,//index
  profilePicUrl: String,//index
  location: {
    state: String,//index
    country: String,//index
    city: String,//index
    latitude: String,
    longitude: String
  },
  engagementIntelligence: [{
    type: String
  }],
  company: String,//index
  connectionsFrom: [{
    name: {
      type: String,
      enum: ['gmail', 'linkedIn', 'twitter', 'apple', 'outlook']
    }
  }],
  notes: [{//index
    text: String,
    date: Date
  }],
  relationship: String,
  bio: String,//index
  professions: [{
    company: String,
    designation: String,
    description: String,
    startDate: String,
    endDate: String
  }],
  favorite: {
    type: Boolean,
    default: false
  },
  indexLabels: [{type: String}],
  tags: [{type: String}], //index
  twitter: {
    handle: String,
    profileUrl: String,
    recentTweet: {
      link: String,
      title: String,
      date: Date 
    }
 },
  linkedIn: {
    profileUrl: String,
    recentPost: {
      link: String,
      title: String,
      date: Date 
    }
  }
}, { timestamps: true });

const Contacts = mongoose.model('contacts', contactsSchema);
module.exports = Contacts;