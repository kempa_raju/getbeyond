
const mongoose = require('mongoose');

const interactionsSchema = new mongoose.Schema({
  ownerEmailId: String,
  contactEmailId: String,
  interactionType: [{
    type: String,
    enum: ['twitter', 'linkedIn', 'gmail', 'outlook']
  }],
  subject: String,
  interactionText: String,
  interactionDate: Date,
  lastDataSyncDate: Date,
  dataSyncDate: String,
  refId: String,
  threadId: String
}, { timestamps: true });

const Users = mongoose.model('interactions', interactionsSchema);

module.exports = Users;