
const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  emailId: String,
  handle: String,
  firstName: String,
  lastName: String,
  mobileNumber: String,
  company: String,
  profilePicUrl: String,
  location: {
    city: String,
    latitude: String,
    longitude: String
  },
  socialLogins: [{
    name: {
      type: String,
      enum: ['gmail', 'linkedIn', 'twitter', 'apple', 'outlook']
    },
    linked: {
      type: Boolean,
      default: false
    },
    accessToken: String,
    refreshToken: String
  }],
  interests: [{type: String}],
  completedActivities: [{
    name: String,
    completed: {
      type: Boolean,
      default: false
    }
  }]
}, { timestamps: true });

const Users = mongoose.model('users', usersSchema);

module.exports = Users;