const express = require('express')
const router = express.Router()
const async = require("async");
const _ = require("lodash")

const contacts = require('../controllers/contacts');
const contactsController = new contacts();

router.get('/contacts/list', handleContactsList);
router.post('/contacts/details', handleContactsDetails);

function handleContactsList(req, res) {
   const searchString = req.query.search;
   const ownerEmailId = req.query.ownerEmailId;

    const projection = {
        firstName: 1,
        lastName: 1,
        profilePicUrl: 1,
        bio: 1,
        company: 1,
        favorite: 1
    }

   if (searchString) {
    contactsController.searchContacts(searchString, projection, (searchErr, searchResult) => {
        if (searchErr || !searchResult) {
            res.send({
                msg: 'Error in getting search results',
                contacts: []
            })
        } else {
            res.send({
                msg: 'Search results obtained successfully',
                contacts: searchResult
            })
        }
    }) 
   } else if(ownerEmailId) {
       const query = buildContactsQuery(req.query);

       contactsController.listContacts(query, projection, (contactsListErr, contactsList) => {
            if (contactsListErr || !contactsList) {
                res.send({
                    msg: 'Error in getting contacts list',
                    contacts: []
                })
            } else {
                res.send({
                    msg: 'Successfully got contacts list',
                    contacts: contactsList
                })
            }
       })
   } else {
       res.status(400);
       res.send({
           msg: 'Please send owner email Id',
           contacts: []
       })
   }

}

function handleContactsDetails(req, res) {
    const emailId = req.body.emailId;

    contactsController.contactDetails(emailId, {}, (contactsErr, contact) => {
        if (contactsErr || !contact) {
            res.send({
                msg: 'Error in getting contact details',
                contact: {}
            })
        } else {
            res.send({
                msg: 'Successfully got contact details',
                contact: contact
            })
        }
    }) 
}

function buildContactsQuery(params) {
    const query = {
        ownerEmailId: params.ownerEmailId
    };

    if (params.favorite) {
        query['favorite'] = true;
    }

    return query;
}

module.exports = router;