const ContactsModel = require('../models/contacts');
function Contacts() {}

Contacts.prototype.searchContacts = (searchString, projection, callback) => {
    ContactsModel.find({$text: {
        $search: searchString
    }}, projection).limit(50).exec(callback);
}

Contacts.prototype.listContacts = (findQuery, projection, callback) => {
    ContactsModel.find(findQuery, projection).limit(50).exec(callback);
}

Contacts.prototype.contactDetails = (emailId, projection, callback) => {
    ContactsModel.findOne({emailId: emailId}, projection, callback)
}

module.exports = Contacts;