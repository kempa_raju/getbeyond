const mongoClient = require('mongodb').MongoClient;

function generatePrivateContacts() {
    mongoClient.connect("mongodb://localhost:27017/getbeyond", function(error, client) {
        if (error)
            throw error;
        else {
            const db = client.db('getbeyond');
            const contactsCollection = db.collection('contacts');
            const publicContactsCollection = db.collection('publicContacts');

            const publicContacts = [];
            contactsCollection.find({}).toArray(function(err, contacts) {
                contacts = contacts || [];

                contacts.forEach(el => {
                    publicContacts.push({
                        emailId: el.emailId,
                        engagementIntelligence: el.engagementIntelligence,
                        connectionsFrom: el.connectionsFrom,
                        relationship: el.relationship,
                        professions: el.professions,
                        favorite: el.favorite,
                        indexLabels: el.indexLabels,
                        twitter: el.twitter,
                        linkedIn: el.linkedIn
                    })
                })

                publicContactsCollection.insertMany(publicContacts, (err, result) => {
                    if (err) {
                        console.log('Error in inserting data');
                    } else {
                        console.log('Done inserting public contacts');
                    }

                    process.exit(0);
                });
            })
        }
    })

}

generatePrivateContacts();