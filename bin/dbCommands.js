db.contacts.createIndex({
    firstName: "text",
    lastName: "text",
    emailId: "text",
    mobileNumber: "text",
    company: "text",
    "notes.text": "text",
    bio: "text",
    tags: "text",
    "location.state": "text",
    "location.country": "text",
    "location.city": "text"
})

db.privateContacts.createIndex({
    firstName: "text",
    lastName: "text",
    emailId: "text",
    mobileNumber: "text",
    company: "text",
    "notes.text": "text",
    bio: "text",
    tags: "text",
    "location.state": "text",
    "location.country": "text",
    "location.city": "text"
})