const mongoClient = require('mongodb').MongoClient;

function generatePrivateContacts() {
    mongoClient.connect("mongodb://localhost:27017/getbeyond", function(error, client) {
        if (error)
            throw error;
        else {
            const db = client.db('getbeyond');
            const contactsCollection = db.collection('contacts');
            const privateContactsCollection = db.collection('privateContacts');

            const privateContacts = [];
            contactsCollection.find({}).toArray(function(err, contacts) {
                contacts = contacts || [];

                contacts.forEach(el => {
                    privateContacts.push({
                        firsName: el.firstName,
                        lastName: el.lastName,
                        emailId: el.emailId,
                        mobileNumber: el.mobileNumber,
                        location: el.location,
                        company: el.company,
                        notes: el.notes,
                        bio: el.bio,
                        tags: el.tags
                    })
                })

                privateContactsCollection.insertMany(privateContacts, (err, result) => {
                    if (err) {
                        console.log('Error in inserting data');
                    } else {
                        console.log('Done inserting private contacts');
                    }

                    process.exit(0);
                });
            })
        }
    })

}

generatePrivateContacts();