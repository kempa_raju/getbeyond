const mongoClient = require('mongodb').MongoClient;
const chance = require('chance').Chance();
const txtgen = require('txtgen');

DATA_SIZE = 20000
DATA_SIZE_ENGAGEMENT_INTELLIGENCE = 3
DATA_SIZE_CONNECTIONS_FROM = 5

const dataEngagementIntelligence = ['Mostly Friendly', 'Perfectionist', 'Self Decipline'];
const dataConnectionsFrom = ['gmail', 'linkedIn', 'twitter', 'apple', 'outlook']

function generateContacts() {
    mongoClient.connect("mongodb://localhost:27017/getbeyond", function(error, client) {
        if (error)
            throw error;
        else {
            const db = client.db('getbeyond');
            const contacts = db.collection('contacts');

            const bulk = contacts.initializeUnorderedBulkOp();
            const contactsArr = [];

            console.log('Beginning to generate data: ', DATA_SIZE);
            for(let i=1; i<=DATA_SIZE; i++) {
                console.log('Generating contacts:', i);
                contactsArr.push({
                    firstName: chance.first(),
                    lastName: chance.last(),
                    emailId: chance.email(),
                    mobileNumber: chance.phone(),
                    profilePicUrl: chance.avatar({protocol: 'https'}),
                    location: {
                        city: chance.city(),
                        latitude: chance.latitude(),
                        longitude: chance.longitude()
                    },
                    engagementIntelligence: getEngagementIntelligence(),
                    company: chance.company(),
                    connectionsFrom: getConnectionsFrom(),
                    notes: getNotesArray(getRandomNumber(10)),
                    bio: txtgen.paragraph(),
                    professions: getDesignations(getRandomNumber(10)),
                    favorite: chance.bool(),
                    favoriteLabel: function() {
                        return this.favorite;
                    },
                    indexLables: [chance.bool() ? 'favorite' : '' ],
                    tags: getTags(getRandomNumber(10)),
                    twitter: {
                        handle: chance.twitter(),
                        profileUrl: chance.url(),
                        recentTweet: {
                            link: chance.avatar({protocol: 'https'}),
                            title: txtgen.sentence(),
                            date: chance.date()
                        }
                    },
                    linkedIn: {
                        profileUrl: chance.url(),
                        recentPost: {
                            link: chance.avatar({protocol: 'https'}),
                            title: txtgen.sentence(),
                            date: chance.date()
                        }
                    }
                })
            }

            console.log('Inserting the contacts into db');
            contacts.insertMany(contactsArr, (err, result) => {
                if (err) {
                    console.log('Error in inserting data');
                } else {
                    console.log('Done inserting contacts');
                }

                process.exit(0);
            });
        }
    })

}

function getEngagementIntelligence() {
    const size = getRandomNumber(DATA_SIZE_ENGAGEMENT_INTELLIGENCE);
    const intelligene = [];

    for (i=0; i<size; i++) {
        intelligene.push(dataEngagementIntelligence[getRandomNumber(DATA_SIZE_ENGAGEMENT_INTELLIGENCE)])
    }

    return intelligene;
}

function getConnectionsFrom() {
    const size = getRandomNumber(DATA_SIZE_CONNECTIONS_FROM);
    const connections = [];

    for (i=0; i<size; i++) {
        connections.push(dataConnectionsFrom[getRandomNumber(DATA_SIZE_CONNECTIONS_FROM)])
    }

    return connections;
}

function getNotesArray(num) {
    const notesArr = []
    for(i=0; i<num; i++) {
        notesArr.push(txtgen.paragraph())
    }

    return notesArr;
}

function getDesignations(num) {
    const designations = [];

    for(i=0; i<num; i++) {
        designations.push({
            company: chance.company(),
            designation: chance.profession(),
            description: txtgen.sentence(),
            startDate: chance.date(),
            endDate: chance.date()
        })
    }

    return designations;
}

function getTags(num) {
    const tags = [];

    for (i=0; i<=num; i++) {
        tags.push(chance.word({length: 5}))
    }
}

function getRandomNumber(maxNum) {
 return Math.floor(Math.random() * maxNum)
}

generateContacts();